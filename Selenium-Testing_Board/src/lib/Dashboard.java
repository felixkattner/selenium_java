package lib;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Dashboard {

	public static void createNewVideo(String title, WebDriver driver) {
		Helper.waitForBlockUI(driver);
		driver.findElement(By.cssSelector("div.visual")).click();
		driver.findElement(
				By.cssSelector("div.projectName.ng-pristine.ng-valid-maxlength.ng-valid.ng-valid-required > input"))
				.sendKeys(title);
		driver.findElement(By.cssSelector(
				"#popupOverlay > div > div > div.popup.projectTypePicker.size-s > div.popup-body > div > div.ng-pristine.ng-untouched.ng-valid.ng-empty > div > div > div:nth-child(1) > div > button:nth-child(2)"))
				.click();
	}

}
