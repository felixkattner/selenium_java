package lib;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Tutorial {
	public static void closeTutorial(WebDriver driver) {
		Helper.waitForBlockUI(driver);
		if (driver.findElement(By.cssSelector("body > div.main > div > section > div > div > div > header > button"))
				.isDisplayed()) {
			driver.findElement(By.cssSelector("body > div.main > div > section > div > div > div > header > button")).click();
		}
	}

}
