package lib;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
* Die Startseite
* @example
* import lib.Startpage;
*/
  /**
  * Login Layer
  */

public class Startpage {	
	
	public static class Login {
		
		public static void loginWithMail(WebDriver driver, String mail, String password) throws InterruptedException {
			cookiesLayer.confirm(driver);
			driver.findElement(By.cssSelector("#main-header button#openLogin")).click();
			driver.findElement(By.xpath("//*[@id=\"2993\"]/form[1]/div[2]/input")).sendKeys(mail);
			driver.findElement(By.xpath("//*[@id=\"2993\"]/form[1]/div[3]/input")).sendKeys(password);
			driver.findElement(By.xpath("//*[@id=\"2993\"]/form[1]/button")).click();
			Helper.waitForBlockUI(driver);
		}
		
	}

}
