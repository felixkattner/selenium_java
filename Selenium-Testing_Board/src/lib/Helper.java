package lib;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helper {

	public static void waitForInvisibility(WebElement webElement, int maxSeconds) {
		Long startTime = System.currentTimeMillis();

		try {
			while (System.currentTimeMillis() - startTime < maxSeconds * 1000 && webElement.isDisplayed()) {
			}
		} catch (StaleElementReferenceException e) {
			return;
		} catch (TimeoutException t) {
			System.out.println("BlockUI länger als 20 Sekunden visible");
			System.exit(-1);
		}
	}

	public static void waitForBlockUI(WebDriver driver) {

		try {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.block-ui-container")));
		waitForInvisibility((driver.findElement(By.cssSelector("div.block-ui-container"))), 20);
		} catch (TimeoutException t){
			return;
		}
	}
}