package lib;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
* Layer zur Bestätigung von Cookies (DSGVO)
* @example
*/

/**
* Akzeptiert die funktionellen Cookies und bestätigt den Cookies Layer
* @param {object} WebDriver: Ein Webdriver wie CHromeDriver oder geckodriver
* @example
* cookiesLayer.confirm(driver);
*/

public class cookiesLayer {
	
	public static void confirm(WebDriver driver) {
		if(driver.findElement(By.xpath("//*[@id=\"4857\"]/div[3]/button")).isDisplayed()) {
			driver.findElement(By.xpath("//*[@id=\"4857\"]/div[3]/button")).click();
		}
		
	}

}
