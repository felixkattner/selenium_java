import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import lib.cookiesLayer;
import static org.junit.Assert.assertTrue;
 
public class testChrome {
	
	static WebDriver driver;
 
    public static void main(String[] args) {
 
        String pathToChromeDriver = ".//Drivers//chromedriver79.exe";
        System.setProperty("webdriver.chrome.driver", pathToChromeDriver);
 
        driver = new ChromeDriver();
        driver.get("https://www.mysimpleshow.com/de/");
        
        /* Identify Element and take action with two lines of code */
        cookiesLayer.confirm(driver);
        
        assertTrue(driver.getCurrentUrl().contains("simpleshow"));
        
    }
}
