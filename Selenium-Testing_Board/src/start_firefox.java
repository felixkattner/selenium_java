import static org.junit.Assert.assertTrue;
import lib.cookiesLayer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class start_firefox {
	
	static WebDriver driver;
	
    public static void main(String[] args) {
    	String pathToFirefoxDriver = ".//Drivers//geckodriver.exe";
        System.setProperty("webdriver.gecko.driver", pathToFirefoxDriver);
 
        driver = new FirefoxDriver();
        driver.get("https://www.mysimpleshow.com/de/");
        
        cookiesLayer.confirm(driver);
        
        assertTrue(driver.getCurrentUrl().contains("simpleshow"));
        
    }
}