import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class initBrowser {
	
	public static WebDriver init_Browser(String[] args, WebDriver driver) throws Exception {
		String browser = null;
		
		if(args.length == 0) {
			System.out.println("No Browser Parameter given, use default Browser Chrome");
			browser = "Chrome";
		} else {
			browser = args[0];
		}
		if(browser.equalsIgnoreCase("Firefox")) {
			String pathToFirefoxDriver = ".//Drivers//geckodriver.exe";
	        System.setProperty("webdriver.gecko.driver", pathToFirefoxDriver);
	        
	        driver = new FirefoxDriver();
		} else if(browser.equalsIgnoreCase("Chrome")) {
			String pathToChromeDriver = ".//Drivers//chromedriver79.exe";
	        System.setProperty("webdriver.chrome.driver", pathToChromeDriver);
	        
	        driver = new ChromeDriver();
		} else {
			System.out.println("Browser not defined!");
			throw new Exception("Browser not defined!");
		}
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		return driver;
	}
}
