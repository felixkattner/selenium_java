import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;

import lib.Dashboard;
import lib.Startpage;
import lib.Tutorial;
import lib.cookiesLayer;
import lib.Phase;

public class Basic {

	public static WebDriver driver;

	public static void main(String[] args) throws Exception {
		String date = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss").format(new Date());
		
		driver = initBrowser.init_Browser(args, driver);
		driver.get("https://www.mysimpleshow.com/de/");
		cookiesLayer.confirm(driver);
		Startpage.Login.loginWithMail(driver, "dennis.fiehn+bot@simpleshow.com", "testuser");
		// AccountSwitcher.choose_account(driver);
		Dashboard.createNewVideo("Selenium " + date, driver);
		Tutorial.closeTutorial(driver);
		Phase.Draft.chooseTemplate(driver);
		Tutorial.closeTutorial(driver);
		driver.close();

	}
}
